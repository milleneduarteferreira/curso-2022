const sections = [
  {
    title: 'Aula 0 - Introdução',
    content: aula00_content,
    resources: [
      {
        text: 'Rede Linux - Sobre',
        link: 'https://www.linux.ime.usp.br/?about'
      },
      {
        text: 'Rede Linux - Regras da Rede',
        link: 'https://www.linux.ime.usp.br/?rules'
      }
    ]
  },
  {
    title: 'Aula 1 - Hardware',
    content: '<p>Em Breve.</p>',
    resources: [
      {
        
      }
    ]
  },
  {
    title: 'Aula 2 - Conceitos de Software Livre',
    content: aula02_content,
    resources: [
      {
        
      }
    ]
  },
  {
    title: 'Aula 3 - Ambiente Linux, CLI e Organização de Arquivos e Diretórios no Linux',
    content: '<p>Em Breve.</p>',
    resources: [
      {
        
      }
    ]
  },
  {
    title: 'Aula 4 - Camadas do Computador e Tarefas Executadas pelo Kernel',
    content: '<p>Em Breve.</p>',
    resources: [
      {
        
      }
    ]
  },
  {
    title: 'Aula 5 - Segurança',
    content: '<p>Em Breve.</p>',
    resources: [
      {
        
      }
    ]
  },
  {
    title: 'Aula 6 - Protocolos e Infraestrutura de Redes',
    content: '<p>Em Breve.</p>',
    resources: [
      {
        
      }
    ]
  },
  {
    title: 'Aula 7 - Versionamento, Sistemas de Pacotes, Serviços Unix e Configuração Básica de Redes',
    content: '<p>Em Breve.</p>',
    resources: [
      {
        
      }
    ]
  },
  {
    title: 'Aula 8 - Mail Server, Web Server, RAID, Containers e Virtualização',
    content: '<p>Em Breve.</p>',
    resources: [
      {
        
      }
    ]
  },
];

function onLoad() {
document.querySelector('#navbar ul').innerHTML = sections.map((section) => {
  return `<li>
    <a href="#${section.title.split(' ').join('_')}" target="_self" class="nav-link">${section.title}</a>
  </li>`;
}).join('');

document.querySelector('main').innerHTML = sections.map((section) => {
  return `<section class="main-section" id="${section.title.split(' ').join('_')}">
    <header>
      <h2>${section.title}</h2>
    </header>
    <div class="container">
      <div class="content">${section.content}</div>
      <h3>Veja mais em:</h3>
      <ul class="resources">${section.resources.map((resource) => `<li><a href="${resource.link}" target="_blank">${resource.text}</a></li>`).join('')}</ul>
    </div>
  </section>`;
}).join('');

document.querySelector('.dropdown-button').addEventListener('click', () => {
  const dropdownIcon = document.querySelector('.dropdown-icon');
  
  if (dropdownIcon.innerHTML === 'menu') {
    dropdownIcon.innerHTML = 'close';
  }
  else {
    dropdownIcon.innerHTML = 'menu';
  }
  
  document.querySelector('nav .nav-links').classList.toggle('show');
});

document.querySelector('.current-year').innerHTML = new Date().getFullYear();
};
